"""

Base class for Exceptions from Exchanges

"""
from abc import ABCMeta, abstractmethod
from ErrorConstants import *


class ExchangeException(Exception):
    __metaclass__ = ABCMeta

    def __init__(self, code):
        if code:
            self.code = code
        else:
            self.code = ErrorConstants.EXCHANGE_ERROR

        self.message = "Error occured in the Exchange API"
